(ns lego.utils)

(def base-url
  (let [loc (.-location js/window)
        protocol (.-protocol loc)
        hostname (.-hostname loc)
        port (.-port loc)]
    (str protocol "//" hostname ":" port)))
