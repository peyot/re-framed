(ns lego.re-frame
  (:require [lego.ajax :as http]
            [re-frame.core :as re-frame.core]
            [schema.core :as s]
            [taoensso.timbre :as log]))


(declare dispatch dispatch-sync)

(def enable-handler-debugging? false)

(def http-method-map
  {:get http/GET
   :post http/POST
   :delete http/DELETE})

;; ---------------------------------------------------------------------
;; Helper Functions

(defn check-namespace-keyword [ky]
  (when-not (namespace ky)
    (log/error "Subscription and handlers keys must be namespaced: " ky)))

(defn wrap-meta [event]
  (let [{:keys [trans-id]
         :or {trans-id (str (gensym "trans"))}
         :as trans-data} (or (meta event) {})
        id (str (gensym "event"))
        new-meta (assoc trans-data
                        :event-id id
                        :trans-id trans-id)]
    (with-meta event new-meta)))

(defn check-schema [val schema ctx]
  (when schema
    (let [check (s/check schema val)]
      (when (some? check)
        (log/error (str "schema problem - " ctx " - " (pr-str check)))))))

(defn http-handler [event http]
  (let [{:keys [uri method on-success on-error
                params format keywords response-format
                req-schema res-schema]
         :or {format :transit
              keywords true
              response-format :transit} :as opts} http
        call (http-method-map method http/GET)
        {:keys [trans-id event-id]} (or (meta event) {})
        handler (fn [res]
                  (check-schema res res-schema [(first event) uri params])
                  (when on-success
                    (let [on-success (with-meta on-success
                                       {:trans-id trans-id
                                        :parent-id event-id})]
                      (dispatch-sync (conj on-success res)))))
        error-handler (fn [res]
                        (if on-error
                          (let [on-error (with-meta on-error
                                           {:trans-id trans-id
                                            :parent-id event-id})]
                            (dispatch-sync (conj on-error res)))
                          (log/info "Error in http call: " opts
                                    " response: " res)))]
    (try
      (when (and req-schema)
        (s/validate req-schema params))
      (call uri
            {:params params
             :format format
             :response-format response-format
             :keywords true
             :handler handler
             :error-handler error-handler})
      (catch js/Error e
        (log/error "Sending http call failed: " (first event) uri params)
        (log/info (pr-str (ex-data e)))))))


;; ---------------------------------------------------------------------
;; Dispatch & Subscribe

(defn dispatch [event]
  (re-frame.core/dispatch (wrap-meta event)))

(defn dispatch-n [events]
  (doseq [event events]
    (dispatch event)))

(defn dispatch-sync [event]
  (re-frame.core/dispatch-sync (wrap-meta event)))

(def subscribe re-frame.core/subscribe)


;; ---------------------------------------------------------------------
;; Event Interceptors


(def http-interceptor
  (re-frame.core/->interceptor
   :id ::http-interceptor
   :after (fn http-interceptor [ctx]
            (let [http (re-frame.core/get-effect ctx :http)
                  event (re-frame.core/get-coeffect ctx :event)]
              (if (map? http)
                (http-handler event http)
                (mapv (partial http-handler event) http)))
            ctx)))

(def schema-post-validation
  (re-frame.core/->interceptor
   :id ::schema-post-validation
   :after (fn schema-val [ctx]
            (let [db (re-frame.core/get-effect ctx :db)
                  schema (re-frame.core/get-effect ctx :schema)
                  event (re-frame.core/get-coeffect ctx :event)]
              (when (and schema db) (check-schema db schema event)))
            ctx)))

(def dispatch-interceptor
  (re-frame.core/->interceptor
   :id ::dispatch-interceptor
   :after (fn dispatch-trans [ctx]
            (let [event (re-frame.core/get-coeffect ctx :event)
                  dispatch-later (re-frame.core/get-effect ctx :dispatch-later)
                  dispatch-s (re-frame.core/get-effect ctx :dispatch)
                  dispatch-n (re-frame.core/get-effect ctx :dispatch-n)
                  dispatch-list (if dispatch-s [dispatch-s] dispatch-n)
                  {:keys [trans-id event-id]} (meta event)]
              (when-not (empty? dispatch-list)
                (doseq [event dispatch-list]
                  (if-not (vector? event)
                    (log/error "re-frame.core: ignoring bad :dispatch value. Expected a vector:" event)
                    (dispatch (with-meta event {:trans-id trans-id :parent-id event-id})))))
              (when dispatch-later
                (doseq [{:keys [ms event] :as effect} dispatch-later]
                  (if (or (empty? event) (not (number? ms)))
                    (log/error "re-frame.core: ignoring bad :dispatch-later value:" effect)
                    (js/window.setTimeout #(dispatch (with-meta event
                                                       {:trans-id trans-id
                                                        :parent-id event-id})) ms)))))
            ctx)))


;;Clear FX Handlers since we use an interceptor now
(re-frame.core/clear-fx :dispatch)
(re-frame.core/clear-fx :dispatch-later)
(re-frame.core/clear-fx :dispatch-n)


(defn reg-event-fx
  ([event-key handler]
   (reg-event-fx event-key [] handler))
  ([event-key interceptors handler]
   (check-namespace-keyword event-key)
   (re-frame.core/reg-event-fx
    event-key
    (remove nil?
            (conj interceptors
                  schema-post-validation
                  http-interceptor
                  dispatch-interceptor
                  (when enable-handler-debugging?
                    re-frame.core/debug)))
    handler)))

(defn reg-event-db
  [event-key handler]
  (re-frame.core/reg-event-db event-key handler))

;; ---------------------------------------------------------------------
;; Registering Subscriptions

(defn reg-sub [sub-key & args]
  (check-namespace-keyword sub-key)
  (apply re-frame.core/reg-sub sub-key args))


;; ---------------------------------------------------------------------
;; Null FX hanlders

(def noop (fn [val] val))

(re-frame.core/reg-fx :schema noop)
(re-frame.core/reg-fx :http noop)
(re-frame.core/reg-fx :dispatch noop)
(re-frame.core/reg-fx :dispatch-n noop)
(re-frame.core/reg-fx :dispatch-later noop)


;; ---------------------------------------------------------------------
;; Effect Handlers

(defn reg-fx [fx-key fx-fn]
  (re-frame.core/reg-fx fx-key fx-fn))
