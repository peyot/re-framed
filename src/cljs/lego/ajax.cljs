(ns lego.ajax
  (:require [ajax.core :as ajax]
            [ajax.protocols :as ajax.protocols]))


(def interceptors (atom []))

(defn register-interceptor! [interceptor]
  (swap! interceptors conj interceptor))

(defn add-interceptor [opts]
  (if (empty? @interceptors)
    opts
    (update opts
            :interceptors
            (fnil into [])
            @interceptors)))

(defn GET [url opts]
  (ajax/GET url
            (-> opts
                add-interceptor)))

(defn POST [url opts]
  (ajax/POST url
             (-> opts
                 add-interceptor)))

(defn DELETE [url opts]
  (ajax/DELETE url
               (-> opts
                   add-interceptor)))
