(ns re-framed.core
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [re-frisk.core :refer [enable-re-frisk!]]
            [re-framed.events]
            [re-framed.subs]
            [re-framed.routes :as routes]
            [re-framed.main-view.views :as views]
            [re-framed.config :as config]
            [re-framed.main-view.handlers :as main.handlers]))


(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (enable-re-frisk!)
    (println "dev mode")))

(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (reagent/render [views/main]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (routes/app-routes)
  (re-frame/dispatch-sync [:initialize-db])
  (re-frame/dispatch-sync [::main.handlers/init-db])
  (re-frame/dispatch-sync [::main.handlers/fetch-beers 0])
  (dev-setup)
  (mount-root))
