(ns re-framed.main-view.views
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [clojure.string :as string]
            [re-framed.main-view.subs :as main.subs]
            [re-framed.main-view.handlers :as main.handlers]))

(defn hero
  []
  (fn []
    [:section.hero
     [:div.hero-body
      [:div.container
       [:h1 "Welcome to the CENX Beer Catalogue"]]]]))

(defn footer
  []
  (fn []
    [:footer.footer
     [:div.container
      [:div.content.has-text-centered
       [:p "CENX Beer Catalogue"]]]]))

(defn paginator
  []
  (let [next-page-enabled? (re-frame/subscribe [::main.subs/next-page-enabled?])
        prev-page-enabled? (re-frame/subscribe [::main.subs/prev-page-enabled?])]
    (fn []
      [:nav.pagination
       [:button.pagination-previous
        {:onClick (fn [_] (re-frame/dispatch [::main.handlers/previous-page]))
         :disabled (not @prev-page-enabled?)}
        "Previous"]
       [:button.pagination-next
        {:onClick (fn [_] (re-frame/dispatch [::main.handlers/next-page]))
         :disabled (not @next-page-enabled?)}
        "Next Page"]])))

(defn main
  []
  (let [beers (re-frame/subscribe [::main.subs/beers])]
    (fn []
      [:div
       [hero]
       [:div.section [paginator]]
       (into [:div.container.is-fluid]
             (mapv (fn [partition]
                     (into [:div.columns]
                           (mapv (fn [beer]
                                   [:div.column
                                    [:div.card
                                     [:div.card-image
                                      [:figure.image
                                       [:img {:src (if-let [labels (get beer "labels")]
                                                     (get labels "medium")
                                                     "https://vignette1.wikia.nocookie.net/phobia/images/2/2e/Beer.jpg/revision/latest?cb=20161108052500")}]]]
                                     [:div.card-content
                                      [:div.media
                                       [:div.media-content
                                        [:p.title.is-4 (get beer "nameDisplay")]
                                        [:p.subtitle.is-6 (get-in beer ["style" "category" "name"])]]]
                                      [:div.content
                                       (get beer "description")]]]])
                                 partition)))
                   (partition-all 4 @beers)))
       [footer]])))
