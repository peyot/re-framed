(ns re-framed.main-view.subs
  (:require [lego.re-frame :as re-frame]
            [re-framed.main-view.db :as db]))

(defn beers
  [db _]
  (get-in db [db/db-key :beers]))

(defn next-page-enabled?
  [db _]
  (let [num-pages (get-in db [db/db-key :num-pages])
        page (get-in db [db/db-key :page])]
    (< page num-pages)))

(defn prev-page-enabled?
  [db _]
  (let [page (get-in db [db/db-key :page])]
    (> page 1)))

(re-frame/reg-sub ::beers beers)
(re-frame/reg-sub ::next-page-enabled? next-page-enabled?)
(re-frame/reg-sub ::prev-page-enabled? prev-page-enabled?)
