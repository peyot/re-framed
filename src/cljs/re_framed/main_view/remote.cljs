(ns re-framed.main-view.remote
  (:require [lego.ajax :refer [GET POST DELETE]]
            [re-frame.core :as re-frame]
            [schema.core :as s]
            [cemerick.url :refer [url-encode]]))

(def api-key "6002e68109c5614f04bced05616b9eb5")

(def uri "/beers")

(defn fetch-beers [page on-success]
  {:method :get
   :uri uri
   :params {:key api-key
            :abv 5
            :p page}
   :on-success on-success})
