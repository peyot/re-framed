(ns re-framed.main-view.db
  (:require [schema.core :as s]
            [re-frame.db :as db]))

(def db-key ::main)

(def default-db
  {:page 1
   :beers []
   :num-pages 0})

(defn initialize-db [db]
  (assoc db
         db-key
         default-db))
