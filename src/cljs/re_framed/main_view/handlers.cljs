(ns re-framed.main-view.handlers
  (:require [taoensso.timbre :as log]
            [clojure.string :as str]
            [lego.re-frame :as re-frame]
            [schema.core :as s]
            [re-framed.main-view.remote :as main.remote]
            [re-framed.main-view.db :as db]))

(defn init-db [{:keys [db]} _]
  {:db (db/initialize-db db)})

(defn fetch-beers [{:keys [db]} [_]]
  (let [page (get-in db [db/db-key :page])]
    {:db db
     :http (main.remote/fetch-beers page [::update-beers])}))

(defn update-beers [{:keys [db]} [_ beers]]
  (js/console.debug "BEERS-->" beers)
  {:db (-> db
           (assoc-in [db/db-key :beers] (get beers "data"))
           (assoc-in [db/db-key :num-pages]
                     (get beers "numberOfPages")))})

(defn ppage [{:keys [db]} [_]]
  (let [page (get-in db [db/db-key :page])]
    {:db (-> db
             (update-in [db/db-key :page] dec))
     :dispatch [::fetch-beers]}))

(defn npage [{:keys [db]} [_]]
  (let [page (get-in db [db/db-key :page])]
    {:db (-> db
             (update-in [db/db-key :page] inc))
     :dispatch [::fetch-beers]}))

(re-frame/reg-event-fx ::fetch-beers fetch-beers)
(re-frame/reg-event-fx ::update-beers update-beers)
(re-frame/reg-event-fx ::init-db init-db)
(re-frame/reg-event-fx ::previous-page ppage)
(re-frame/reg-event-fx ::next-page npage)
