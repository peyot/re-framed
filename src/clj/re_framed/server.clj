(ns re-framed.server
  (:require [re-framed.handler :refer [handler]]
            [config.core :refer [env]]
            [ring.adapter.jetty :refer [run-jetty]]
            [figwheel-sidecar.repl-api :as ra])
  (:gen-class))

(defn -main [& args]
  (let [port (Integer/parseInt (or (env :port) "3000"))]
    (run-jetty handler {:port port :join? false})))

(defn start
  []
  (ra/start-figwheel!))
