(ns re-framed.handler
  (:require [compojure.core :refer [GET defroutes]]
            [compojure.route :refer [resources]]
            [compojure.handler :refer [api]]
            [ring.util.response :refer [resource-response response]]
            [ring.middleware.reload :refer [wrap-reload]]
            [org.httpkit.client :as http]))

(defroutes routes
  (GET "/" [] (resource-response "index.html" {:root "public"}))
  (GET "/beers" [] (api
                    (fn [{:keys [query-params] :as ctx}]
                      (response (:body @(http/get (str "http://api.brewerydb.com/v2"
                                                       (:uri ctx))
                                                  {:query-params query-params}))))))
  (resources "/"))

(def dev-handler (-> #'routes wrap-reload))

(def handler routes)
