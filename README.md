# README #

Welcome to `re-framed`, a sample application designed to demonstrate
how we use `re-frame` at CENX.

### Prerequisites ###

* Working knowledge of html/css
* Basic understanding of React.js virtual DOM and lifecycle methods.
* Thorough understanding of the client/server model, HTTP verbs, and AJAX.

### Project Goals ###
* To develop a solid understanding of re-frame, and its underlying
  techonologies.

### Project Non-Goals ###
* To learn or do deep dive into any of the topics listed in the
  prerequisites section.
* To cover or discuss any CENX related bugs/stories

### How do I get set up? ###

* `git clone git@bitbucket.org:peyot/re-framed.git`

* in a terminal window `cd /re-framed` and `lein less auto`
* in emacs start a repl
* once the repl has loaded, go into the `re-framed.server` namespace and run  `(start)`
* in your browser navigate to `http://localhost:3449`

### Who do I talk to? ###

* Peyo Tzolov
* Corey Ling
* Cam MacNeil
